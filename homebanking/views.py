from django.shortcuts import render, redirect, get_object_or_404
from django.http import Http404
# from django.forms.models import model_to_dict
from datetime import datetime
from .forms import FillClientData, ConnexionAccount
from homebanking.models import ClientData

   
def home_page(request):
    form = ConnexionAccount(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            Username = form.cleaned_data['Username']
            Password = form.cleaned_data['Password']
            data = get_object_or_404(ClientData, Username=Username, Password= Password)
            return render(request, 'homebanking/my_account.html', {'data' : data})
        else:
            return HttpResponse("Invalid entry.")
    else:
        return render(request, 'homebanking/home.html', locals())   
    
def my_account(request, id):
    data = get_object_or_404(ClientData, ClientId=id)
    return render(request, 'homebanking/my_account.html', {'data' : data})

def modif_my_account(request, id):
    data = get_object_or_404(ClientData, ClientId=id)
    if request.method == "POST":
        form = FillClientData(request.POST, instance=data)
        if form.is_valid():
            form.save()
            return render(request, 'homebanking/my_account.html', {'data' : data})
        else:
            return HttpResponse("Invalid entry.")
    else:
        form = FillClientData(instance=data)
        return render(request, 'homebanking/modification.html', locals())   

def recupdata(request):
    form = FillClientData(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            Name = form.cleaned_data['Name']
            Firstname = form.cleaned_data['Firstname']
            Adress = form.cleaned_data['Adress']
            Birthdate = form.cleaned_data['Birthdate']
            Number = form.cleaned_data['Number']
            Type = form.cleaned_data['Type']
            Balance = form.cleaned_data['Balance']
            Username = form.cleaned_data['Username']
            Password = form.cleaned_data['Password']
            form.save() 
            data = get_object_or_404(ClientData, Username=Username, Password= Password)
            return render(request, 'homebanking/my_account.html', {'data' : data})
        else:
            return HttpResponse("Invalid entry.")
    else:
          
        return render(request, 'homebanking/creation.html', locals())   

def delete_my_account(request):
    form = ConnexionAccount(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            Username = form.cleaned_data['Username']
            Password = form.cleaned_data['Password']
            data_del = get_object_or_404(ClientData, Username=Username, Password= Password)             
            data_del.delete()
            return render(request, 'homebanking/delete_confirmation.html')
        else:
            return HttpResponse("Invalid entry.")
    else:
        return render(request, 'homebanking/delete.html', locals())          


    
