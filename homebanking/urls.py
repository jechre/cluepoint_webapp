from django.urls import path, include
from django.contrib import admin
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('home/', views.home_page, name = 'connexion'),
    path('mydata/<int:id>', views.my_account),
    path('creation/', views.recupdata, name = 'creation'),
    path('delete/', views.delete_my_account, name = 'delete'),
    path('modification/<int:id>', views.modif_my_account, name = 'modification'),
    
]