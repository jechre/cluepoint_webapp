from django import forms
from .models import ClientData
from datetime import datetime
choices_type = ('Current Account','Visa','Master Card', 'Sparing account', 'AMEX', 'Other')
date_range = 122    
this_year = datetime.now().year
class FillClientData(forms.ModelForm):
    class Meta:
        model = ClientData
        widgets = {
        'Password': forms.PasswordInput(),
        'Birthdate' : forms.SelectDateWidget(years = range(this_year , this_year - date_range, -1))
    }
        
        exclude = ('Creation',)
 

class ConnexionAccount(forms.ModelForm):
    class Meta:
        model = ClientData
        widgets = {
        'Password': forms.PasswordInput(),
    }
        fields = ('Username','Password',)





