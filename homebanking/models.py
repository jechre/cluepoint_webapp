from django.db import models
from django.utils import timezone
from django import forms



class ClientData(models.Model):
    Username = models.CharField(max_length=30)
    Password = models.CharField(max_length=30)
    ClientId = models.AutoField(primary_key=True)
    Name = models.CharField(max_length=42)
    Firstname = models.CharField(max_length=32)
    Adress = models.CharField(max_length=300)
    # Birthday = models.DateField(auto_now=False, auto_now_add=False)
    Birthdate = models.DateField(max_length=12)
    Number = models.CharField(max_length=30, verbose_name="Account Number")
    Type = models.CharField(max_length=42, verbose_name="Account Type") 
    Balance = models.IntegerField(verbose_name="Current account balance")
    Creation = models.DateTimeField(default=timezone.now, 
                                verbose_name="Date creation compte")

    # user= models.ForeignKey('User', on_delete = models.CASCADE)
    class Meta:
        verbose_name = "Client database"
        ordering = ['Creation']
    
    def __str__(self):
        
        return self.Username


# class User(models.Models)
    # username=models.CharField(max_length=30)
    # password=models.CharField(max_length=30)
    
    # def __str__(self)
    # return self.username

