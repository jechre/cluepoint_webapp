from django.apps import AppConfig


class HomebankingConfig(AppConfig):
    name = 'homebanking'
