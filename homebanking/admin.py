from django.contrib import admin

# Register your models here.

from .models import ClientData

admin.site.register(ClientData)
